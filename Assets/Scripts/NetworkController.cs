﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkController : Photon.MonoBehaviour {

    // Use this for initialization

    [SerializeField]
    private GameObject astroMan;
    [SerializeField]
    private Text statusTxt;


    void Start()
    {
        PhotonNetwork.ConnectUsingSettings("0.1");
    }

    private void Update()
    {
        statusTxt.text = PhotonNetwork.room.name;
    }

    public virtual void OnConnectedToMaster()
    {
        Debug.Log("Joined to the Server");
        PhotonNetwork.JoinLobby();
    }

    public virtual void OnJoinedLobby()
    {
        Debug.Log("Joined to the Lobby");
        RoomOptions rmOp = new RoomOptions();
        rmOp.maxPlayers = 3;
        PhotonNetwork.JoinOrCreateRoom("Room1", rmOp, null);
    }

    public virtual void OnJoinedRoom()
    {

        // TODO: Instantiate the camera
        Debug.Log("Joined to the room");
        Debug.Log(PhotonNetwork.room.name+" Number:"+PhotonNetwork.room.playerCount);
        Instantiate(astroMan, new Vector3(0, -0.5f, 1f), Quaternion.identity);


       

    }
}
